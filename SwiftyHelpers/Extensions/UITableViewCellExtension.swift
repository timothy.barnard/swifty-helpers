//
//  UITableViewCellExtension.swift
//  DIT Timetable v3
//
//  Created by Timothy Barnard on 25/01/2018.
//  Copyright © 2018 Timothy Barnard. All rights reserved.
//

import Foundation
import UIKit

extension UITableViewCell: ReusableView { }
extension UITableViewCell: NibLoadableView { }
