//
//  UIColorExtension.swift
//  DIT Timetable v3
//
//  Created by Timothy Barnard on 25/01/2018.
//  Copyright © 2018 Timothy Barnard. All rights reserved.
//

import Foundation
import UIKit

public extension Optional where Wrapped == UIColor {
    
    func defaultValue(_ color: UIColor) -> UIColor {
        if let unwrappedColor = self {
            return unwrappedColor
        } else {
            return color
        }
    }
}

public extension UIColor {

    var getAlpha: (CGFloat)? {
        guard let components = self.cgColor.components else { return nil }
        return components[3]
    }

    var getRed: (CGFloat)? {
        guard let components = self.cgColor.components else { return nil }
        return components[0]
    }

    var getGreen: (CGFloat)? {
        guard let components = self.cgColor.components else { return nil }
        return components[1]
    }

    var getBlue: (CGFloat)? {
        guard let components = self.cgColor.components else { return nil }
        return components[2]
    }
}

public extension UIColor {
    var isLight: Bool {
        var white: CGFloat = 0
        getWhite(&white, alpha: nil)
        return white > 0.5
    }

    var blackOrWhite: UIColor {
        var white: CGFloat = 0
        getWhite(&white, alpha: nil)
        return ( white > 0.5) ? UIColor.black : UIColor.white
    }
}

public extension UIColor {
    
    convenience init(_ hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let redInt = Int(color >> 16) & mask
        let greenInt = Int(color >> 8) & mask
        let blueInt = Int(color) & mask
        let red   = CGFloat(redInt) / 255.0
        let green = CGFloat(greenInt) / 255.0
        let blue  = CGFloat(blueInt) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }

    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0

        var rgbValue: UInt64 = 0

        scanner.scanHexInt64(&rgbValue)

        let red = (rgbValue & 0xff0000) >> 16
        let green = (rgbValue & 0xff00) >> 8
        let blue = rgbValue & 0xff

        self.init(
            red: CGFloat(red) / 0xff,
            green: CGFloat(green) / 0xff,
            blue: CGFloat(blue) / 0xff, alpha: 1
        )
    }

    var toHexString: String {
        var red :CGFloat = 0
        var green :CGFloat = 0
        var blue :CGFloat = 0
        var alpha :CGFloat = 0
        getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        let rgb:Int = (Int)(red*255)<<16 | (Int)(green*255)<<8 | (Int)(blue*255)<<0
        return String(format:"#%06x", rgb)
    }

    func getDarkColor(for hex: String) -> String {
        let testColor = UIColor(hex: hex)

        var red: CGFloat = 0
        var green: CGFloat = 0
        var blue: CGFloat = 0
        var alpha: CGFloat = 0

        testColor.getRed(&red, green: &green, blue: &blue, alpha: &alpha)

        if(red != 0) {
            red *= 0.8
        }
        if(green != 0) {
            green *= 0.8
        }
        if(blue != 0) {
            blue *= 0.8
        }
        return testColor.toHexString
    }
}
