//
//  UICollectionViewCellExtension.swift
//  DIT Timetable v3
//
//  Created by Timothy Barnard on 25/01/2018.
//  Copyright © 2018 Timothy Barnard. All rights reserved.
//

import Foundation
import UIKit

public protocol ReusableView: class {}
public protocol NibLoadableView: class { }
public protocol IndicatableView: class {}

extension UICollectionViewCell: ReusableView { }
extension UICollectionViewCell: NibLoadableView { }
