//
//  UIViewControllerExtension.swift
//  DIT Timetable v3
//
//  Created by Timothy Barnard on 25/01/2018.
//  Copyright © 2018 Timothy Barnard. All rights reserved.
//
import UIKit

public extension UIViewController {

    private struct ActivityView {
        static let spinner = UIActivityIndicatorView(style: .gray)
    }

    /// Added keyboard dimiss gesture to view
    public func addKeyboardDismissGesture() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))

        view.addGestureRecognizer(tap)
    }
    
    public var isSpinnerAnimating: Bool {
        return ActivityView.spinner.isAnimating
    }

    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }

    
    /// Remove all view gestures
    func removeAllGestures() {
        for recognizer in self.view.gestureRecognizers ?? [] {
            self.view.removeGestureRecognizer(recognizer)
        }
    }

    
    /// Show and start activity spinner
    ///
    /// - Parameter isWhite: If you want the activity spinner to be white
    func showAndStartSpinner(_ isWhite: Bool = false) {
        ActivityView.spinner.center = self.view.center
        ActivityView.spinner.scale(factor: 3)
        ActivityView.spinner.startAnimating()
        ActivityView.spinner.style = isWhite ? .whiteLarge : .gray
        ActivityView.spinner.hidesWhenStopped = true
        self.view.addSubview(ActivityView.spinner)
    }

    /// Stop and remove spinner from view
    func stopSpinnerAndRemove() {
        ActivityView.spinner.stopAnimating()
    }
    
    /// Show custom dialog
    ///
    /// - Parameters:
    ///   - leftButtonTitle: Left button title
    ///   - rightButtonTitle: Right button title, optional. if not set then the button is hidden
    ///   - message: The message of the dialog
    ///   - handler: Completion handler with index with the button click. Left: 0, Right: 1
    public func showDialog(_ leftButtonTitle: String, rightButtonTitle: String?, message: String, handler: ((Int) -> Void)? = nil) {
        if let window = UIApplication.shared.keyWindow {
            guard let customDialog = CustomDialog.loadNib() else {return}
            customDialog.frame = window.frame
            customDialog.setLeftButton(leftButtonTitle, color: .red)
            customDialog.setRightButton(rightButtonTitle ?? "", color: .blue)
            customDialog.hideRightButton = rightButtonTitle == nil
            window.addSubview(customDialog)
            
            customDialog.actionClicked = {
                (button: CustomDialog.Button) -> Void in
                handler?(button.index)
                customDialog.removeFromSuperview()
            }
        }
    }
    
    /// Show custom dialog
    ///
    /// - Parameters:
    ///   - customDialog: CustomDialog view, which can be configured
    ///   - handler: Completion handler with index with the button click. Left: 0, Right: 1
    public func showDialog(_ customDialog: CustomDialog, handler: ((Int) -> Void)? = nil) {
        if let window = UIApplication.shared.keyWindow {
            window.addSubview(customDialog)
            customDialog.actionClicked = {
                (button: CustomDialog.Button) -> Void in
                handler?(button.index)
                customDialog.removeFromSuperview()
            }
        }
    }
}
