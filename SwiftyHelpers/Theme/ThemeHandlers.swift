//
//  BaseCollectionViewController.swift
//  SwiftyHelpers
//
//  Created by Timothy on 23/10/2018.
//  Copyright © 2018 Timothy. All rights reserved.
//

import UIKit

/// Theme observer protocol
public protocol ThemeDelegate {
    func themeUpdate()
}

/// ThemeHandler - class to hold the notification observer
public final class ThemeHandler {
    
    /// ThemeDelegate - delegate for call back function
    public var delegate: ThemeDelegate?
    
    /// Notification oberserver
    private var observer: Any?
    
    public init() {
        observer = NotificationCenter.default.addObserver(
            forName: Notification.Name.ThemeChange,
            object: nil,
            queue: OperationQueue.main,
            using: { [self] _ in
                self.delegate?.themeUpdate()
        })
    }
    
    /// Remove the theme notification observer
    public func removeObserver() {
        if let observer = self.observer {
            NotificationCenter.default.removeObserver(observer)
        }
    }
}
