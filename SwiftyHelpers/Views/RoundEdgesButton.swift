//
//  RoundedEdges.swift
//  SwiftyHelpers
//
//  Created by Timothy on 01/11/2018.
//  Copyright © 2018 Timothy. All rights reserved.
//
import UIKit

public class RoundEdgesButton: UIButton {
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        setupLayout()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }
    
    private func setupLayout() {
        addShadow()
    }
    
}
