//
//  FabButton.swift
//  SwiftyHelpers
//
//  Created by Timothy on 01/11/2018.
//  Copyright © 2018 Timothy. All rights reserved.
//
import UIKit

public class FabButton: UIButton {
    
    public override func awakeFromNib() {
        super.awakeFromNib()
        setupLayout()
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }
    
    private func setupLayout() {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        layer.masksToBounds = false
        layer.shadowRadius = 5
        layer.shadowOpacity = 0.8
        layer.cornerRadius = self.frame.width / 2
    }

}
