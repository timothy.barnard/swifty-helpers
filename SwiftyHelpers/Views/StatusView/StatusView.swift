//
//  StatusView.swift
//  SwiftyHelpers
//
//  Created by Timothy on 23/10/2018.
//  Copyright © 2018 Timothy. All rights reserved.
//

import UIKit

/// Status view
public class StatusView: UIView {
    
    /// ImageView, headline and subheading label
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var headlineLabel: UILabel!
    @IBOutlet weak var subheadLabel: UILabel!
    
    let themeHandler = ThemeHandler()
    private let nibName = "StatusView"
    var contentView: UIView!

    // MARK: Set Up View
    public override init(frame: CGRect) {
        // For use in code
        super.init(frame: frame)
        setUpView()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        // For use in Interface Builder
        super.init(coder: aDecoder)
        setUpView()
    }
    
    // Allow view to control itself
    public override func layoutSubviews() {
        // Rounded corners
        self.layoutIfNeeded()
        self.contentView.layer.masksToBounds = true
        self.contentView.clipsToBounds = true
        self.contentView.layer.cornerRadius = 10
    }
    
    private func setUpView() {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: self.nibName, bundle: bundle)
        guard let contentView = nib.instantiate(withOwner: self, options: nil).first as? UIView else {return}
        self.contentView = contentView
        addSubview(contentView)
        
        contentView.center = self.center
        contentView.autoresizingMask = []
        contentView.translatesAutoresizingMaskIntoConstraints = true
        
        statusImage.image = nil
        headlineLabel.text = ""
        subheadLabel.text = ""
    }
    
    // MARK: Provide functions to update view
    public func set(image: UIImage) {
        self.statusImage.image = image
    }
    public func set(headline text: String) {
        self.headlineLabel.text = text
    }
    public func set(subheading text: String) {
        self.subheadLabel.text = text
    }
}
