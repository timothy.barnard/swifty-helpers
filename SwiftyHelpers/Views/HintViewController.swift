//
//  HintViewController.swift
//  SwiftyHelpers
//
//  Created by Timothy on 29/10/2018.
//  Copyright © 2018 Timothy. All rights reserved.
//
import UIKit

public class HintViewController: UIViewController {
    
    var pageName: String = ""
    var hintButtonRight: Bool = true
    
    lazy private var hintButton: UIButton = {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        button.addTarget(self, action: #selector(showHints), for: .touchUpInside)
        button.setTitle("Hints", for: .normal)
        button.setTitleColor(.blue, for: .normal)
        button.addRoundedShadow()
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    override public func viewDidLoad() {
        super.viewDidLoad()
        self.setupHintButton()
    }
    
    private func setupHintButton() {
        if let hints = HintService.getHints(for: pageName), !hints.isEmpty {
            self.view.addSubview(hintButton)
            NSLayoutConstraint.activate([
                hintButton.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 40),
                hintButton.widthAnchor.constraint(equalToConstant: 30),
                hintButton.heightAnchor.constraint(equalToConstant: 30),
                ])
            
            if hintButtonRight {
                hintButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 20).isActive = true
            } else {
                hintButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
            }
            
            UIView.animate(withDuration: 1.0, delay: 0, options: [.repeat, .autoreverse], animations: {
                self.hintButton.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
            }, completion: nil)
        } else {
            self.hintButton.removeFromSuperview()
        }
    }

    
    @objc func showHints() {
        if let hints = HintService.getHints(for: pageName), let hint = hints.first {
            self.showDialog("Helpfull".localized(), rightButtonTitle: "Not helpfull".localized(), message: hint.message) { (index) in
                print(index)
            }
        }
    }
}
