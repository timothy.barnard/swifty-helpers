//
//  CustomDialog1.swift
//  SwiftyHelpers
//
//  Created by Timothy on 25/10/2018.
//  Copyright © 2018 Timothy. All rights reserved.
//

import UIKit

public class CustomDialog: UIView {
    
    public enum Button {
        case left
        case right
        
        public var index: Int {
            switch self {
            case .left: return 0
            case .right: return 1
            }
        }
    }

    /// Message and two buttons
    @IBOutlet weak var mainView: RoundedShadowView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var buttonLeft: UIButton!
    @IBOutlet weak var buttonRight: UIButton!
    
    public var actionClicked: ((_ button: Button) -> Void)?
    
    public static func loadNib() -> CustomDialog? {
        let bundle = Bundle(for: CustomDialog.self)
        let nib = UINib(nibName: "CustomDialog", bundle: bundle)
        return nib.instantiate(withOwner: self, options: nil).first as? CustomDialog
    }
    
    @IBAction func leftButtonPressed(_ sender: UIButton) {
        self.actionClicked?(.left)
    }
    @IBAction func rightButtonPressed(_ sender: UIButton) {
        self.actionClicked?(.right)
    }
    
    // MARK: Provide functions to update view
    public func setLeftButton(_ title: String, color: UIColor) {
        self.buttonLeft.backgroundColor = color
        self.buttonLeft.setTitle(title, for: .normal)
    }
    public func setRightButton(_ title: String, color: UIColor) {
        self.buttonRight.backgroundColor = color
        self.buttonRight.setTitle(title, for: .normal)
    }
    public func setMessage(_ message: String) {
        self.messageLabel.text = message
    }
    public func setBackgroundColor(_ color: UIColor) {
        self.mainView.backgroundColor = color
    }
    public var hideRightButton: Bool = false {
        didSet {
            self.buttonRight.isHidden = hideRightButton
        }
    }

}
