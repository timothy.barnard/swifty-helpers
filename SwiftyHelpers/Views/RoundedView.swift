//
//  RoundedView.swift
//  SwiftyHelpers
//
//  Created by Timothy on 23/10/2018.
//  Copyright © 2018 Timothy. All rights reserved.
//
import UIKit

/// Rounded view with a shadow
public class RoundedShadowView: UIView {
    
    private var shadowLayer: CAShapeLayer!
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        if self.cornerRadius == 0 {
            self.cornerRadius = 10
            self.clipsToBounds = true
        }
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        
        if shadowLayer == nil {
            shadowLayer = CAShapeLayer()
            shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
            shadowLayer.fillColor = UIColor.clear.cgColor
            shadowLayer.shadowColor = UIColor.black.cgColor
            shadowLayer.shadowPath = shadowLayer.path
            shadowLayer.shadowOffset = CGSize(width: 0.0, height: 1.0)
            shadowLayer.shadowOpacity = 0.2
            shadowLayer.shadowRadius = 3
            
            layer.insertSublayer(shadowLayer, at: 0)
        }
    }
}
