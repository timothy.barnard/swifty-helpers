//
//  AlertService.swift
//  SwiftyHelpers
//
//  Created by Timothy on 23/10/2018.
//  Copyright © 2018 Timothy. All rights reserved.
//
import UIKit


/// Alert service
public final class AlertService {
    
    private init() {}
    
    /// Show alert
    ///
    /// - Parameters:
    ///   - vc: Self
    ///   - title: Title of alert dialog
    ///   - message: Message of alert dialog
    ///   - buttons: List of buttons to show
    ///   - allowCancel: Show cancel button
    ///   - completion: completion block to return pressed button index
    static public func addAlert(in vc: UIViewController, title: String?, message: String?, buttons: [String], allowCancel: Bool = true, completion: ((Int) -> Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        for (index, button) in buttons.enumerated() {
            alertController.addAction(UIAlertAction(title: button, style: .default, handler: { (_) in
                completion?(index)
            }))
        }
        if allowCancel {
            alertController.addAction(UIAlertAction(title: "Cancel".localized(), style: .destructive, handler: nil))
        }
        
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = vc.view
            popoverController.sourceRect = CGRect(x: vc.view.bounds.midX, y: vc.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        vc.present(alertController, animated: true, completion: nil)
    }
    
    
    /// Show action sheet
    ///
    /// - Parameters:
    ///   - vc: Self
    ///   - title: Title of alert sheet
    ///   - message: Message of alert dialog
    ///   - buttons: List of buttons to show
    ///   - completion: optional completion block to return pressed button index
    static public func addActionSheet(in vc: UIViewController, title: String?, message: String?, buttons: [String], completion: ((Int) -> Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        for (index, button) in buttons.enumerated() {
            alertController.addAction(UIAlertAction(title: button, style: .default, handler: { (_) in
                completion?(index)
            }))
        }
        
        alertController.addAction(UIAlertAction(title: "Cancel".localized(), style: .destructive, handler: nil))
        
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = vc.view
            popoverController.sourceRect = CGRect(x: vc.view.bounds.midX, y: vc.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        vc.present(alertController, animated: true, completion: nil)
    }
    
    
    /// Alert with text field
    ///
    /// - Parameters:
    ///   - vc: Self
    ///   - title: Title of alert text field dialog
    ///   - message: Message of alert text field dialog
    ///   - completion: completion block return text field value
    static public func addAlertWithTextField(in vc: UIViewController, title: String?, message: String?, completion: ((String) -> Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alertController.addTextField { (textField) in
            textField.placeholder = "Enter value".localized()
        }
        
        alertController.addAction(UIAlertAction(title: "OK".localized(), style: .default, handler: { (_) in
            guard let searchValue = alertController.textFields?.first?.text else {
                return
            }
            completion?(searchValue)
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel".localized(), style: .destructive, handler: nil))
        
        if let popoverController = alertController.popoverPresentationController {
            popoverController.sourceView = vc.view
            popoverController.sourceRect = CGRect(x: vc.view.bounds.midX, y: vc.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        
        vc.present(alertController, animated: true, completion: nil)
    }
    
}
