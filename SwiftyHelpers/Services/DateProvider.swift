//
//  DateProvider.swift
//  SwiftyHelpers
//
//  Created by Timothy on 24/10/2018.
//  Copyright © 2018 Timothy. All rights reserved.
//
import Foundation

/// Date provider class
public class DateProvider {
    
    static public let shared = DateProvider()
    
    private init() {}
    
    private let dateFormatter = DateFormatter()
    
    public func shortTimeString(from date: Foundation.Date) -> String {
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .short
        
        return dateFormatter.string(from: date)
    }
    
    public func mediumTimeString(from date: Foundation.Date) -> String {
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .medium
        
        return dateFormatter.string(from: date)
    }
    
    public func longTimeString(from date: Foundation.Date) -> String {
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .long
        
        return dateFormatter.string(from: date)
    }
    
    public func fullTimeString(from date: Foundation.Date) -> String {
        dateFormatter.dateStyle = .none
        dateFormatter.timeStyle = .full
        
        return dateFormatter.string(from: date)
    }
    
    public func shortDateString(from date: Foundation.Date) -> String {
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .none
        
        return dateFormatter.string(from: date)
    }
    
    public func mediumDateString(from date: Foundation.Date) -> String {
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .none
        
        return dateFormatter.string(from: date)
    }
    
    public func longDateString(from date: Foundation.Date) -> String {
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .none
        
        return dateFormatter.string(from: date)
    }
    
    public func fullDateString(from date: Foundation.Date) -> String {
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
        
        return dateFormatter.string(from: date)
    }
    
    public func shortTimeMediumDateString(form date: Date) -> String {
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .short
        
        return dateFormatter.string(from: date)
    }
}
