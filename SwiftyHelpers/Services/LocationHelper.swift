//
//  LocationHelper.swift
//  SwiftyHelpers
//
//  Created by Timothy on 24/10/2018.
//  Copyright © 2018 Timothy. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

public struct ReversedGeoLocation {
    public let name: String            // eg. Apple Inc.
    public let streetName: String      // eg. Infinite Loop
    public let streetNumber: String    // eg. 1
    public let city: String            // eg. Cupertino
    public let state: String           // eg. CA
    public let zipCode: String         // eg. 95014
    public let country: String         // eg. United States
    public let isoCountryCode: String  // eg. US
    
    public var formattedAddress: String {
        return """
        \(name),
        \(streetNumber) \(streetName),
        \(city), \(state) \(zipCode)
        \(country)
        """
    }
    
    // Handle optionals as needed
    init(with placemark: CLPlacemark) {
        self.name           = placemark.name ?? ""
        self.streetName     = placemark.thoroughfare ?? ""
        self.streetNumber   = placemark.subThoroughfare ?? ""
        self.city           = placemark.locality ?? ""
        self.state          = placemark.administrativeArea ?? ""
        self.zipCode        = placemark.postalCode ?? ""
        self.country        = placemark.country ?? ""
        self.isoCountryCode = placemark.isoCountryCode ?? ""
    }
}

public protocol UserLocation {
    func updateLocation(_ latitude: Float, longitude: Float)
}

public class LocationHelper: NSObject {
    
    public var delegate: UserLocation?
    var locationManager = CLLocationManager()
    
    public override init() {
        self.locationManager.requestWhenInUseAuthorization()
    }
    
    public var isAuthorized: Bool {
         return CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() == .authorizedAlways
    }
    
    public func stopUserLocationUpdates() {
        self.locationManager.stopUpdatingLocation()
    }
    
    public func getUserLocationUpdates() {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    public func requestWhenInUseAuthorization() {
        locationManager.requestWhenInUseAuthorization()
    }
    
    static func getLocationFromFloats(_ latitude: Float, _ longitude: Float) -> CLLocation {
        let latitudeDegress = CLLocationDegrees(latitude)
        let longitudeDegress = CLLocationDegrees(longitude)
        return CLLocation(latitude: latitudeDegress, longitude: longitudeDegress)
    }
    
    static func getLocationFromDegrees(_ latitude: CLLocationDegrees, _ longitude: CLLocationDegrees) -> CLLocation {
        return CLLocation(latitude: latitude, longitude: longitude)
    }
    
    static func getDistanceInMeters(_ coordinateOne: CLLocation, coordinateTwo: CLLocation) -> Int {
        let distanceInMeters = coordinateOne.distance(from: coordinateTwo)
        return Int(distanceInMeters.rounded(toPlaces: 0))
    }
    
    static func getDistanceInMeters(_ coordinateOne: CLLocation, coordinateTwo: CLLocation) -> String {
        let distanceInMeters = coordinateOne.distance(from: coordinateTwo)
        return "\(distanceInMeters.rounded(toPlaces: 0)) m"
    }
    
    static func getDistanceInMiles(_ coordinateOne: CLLocation, coordinateTwo: CLLocation) -> Int {
        let distanceInMeters = coordinateOne.distance(from: coordinateTwo)
        let distanceInMiles = distanceInMeters * 0.00062137
        return Int(distanceInMiles.rounded(toPlaces: 1))
    }
    
    static func getDistanceInMiles(_ coordinateOne: CLLocation, coordinateTwo: CLLocation) -> String {
        let distanceInMeters = coordinateOne.distance(from: coordinateTwo)
        let distanceInMiles = distanceInMeters * 0.00062137
        return "\(distanceInMiles.rounded(toPlaces: 1)) mi"
    }

    public func getAdress(_ location: CLLocation,  completion: @escaping (_ address: ReversedGeoLocation?, _ error: Error?) -> ()) {
        if self.isAuthorized {
            let geoCoder = CLGeocoder()
            geoCoder.reverseGeocodeLocation(location) { placemarks, error in
                if let e = error {
                    completion(nil, e)
                } else {
                    guard let placeMark = placemarks?.first else {
                        completion(nil, nil)
                        return
                    }
                    completion(ReversedGeoLocation(with: placeMark), error)
                }
            }
        }
    }
}

extension LocationHelper: CLLocationManagerDelegate {
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        let coordinations = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude,longitude: userLocation.coordinate.longitude)
        
        let longitude = Float(coordinations.longitude)
        let latitude = Float(coordinations.latitude)
        
        self.delegate?.updateLocation(latitude, longitude: longitude)
    }
}
