//
//  TypeAlias.swift
//  SwiftyHelpers
//
//  Created by Timothy on 24/10/2018.
//  Copyright © 2018 Timothy. All rights reserved.
//
import Foundation

public typealias Closure<T> = (T) -> Void
