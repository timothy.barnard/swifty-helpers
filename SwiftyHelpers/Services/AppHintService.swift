//
//  AppHintService.swift
//  SwiftyHelpers
//
//  Created by Timothy on 27/10/2018.
//  Copyright © 2018 Timothy. All rights reserved.
//

import Foundation

public struct Hint: Codable {
    let message: String
    let screen: String
    var seen: Bool = false
    
    init(message: String, screen: String) {
        self.message = message
        self.screen = screen
    }
}

public class HintService: NSObject {
    
    static let fileName = "hints.json"
    
    static private func encode(_ hints: [Hint]) -> String? {
        let jsonEncoder = JSONEncoder()
        do {
            let jsonData = try jsonEncoder.encode(hints)
            return String(data: jsonData, encoding: .utf8)
        }
        catch {
            print(error)
            return nil
        }
    }
    
    static private func decode(_ hintData: Data) -> [Hint]? {
        do {
            let jsonDecoder = JSONDecoder()
            return try jsonDecoder.decode([Hint].self, from: hintData)
        }
        catch {
            return nil
        }
    }
    
    static func storeHints(_ hints: [Hint]) {
        if let hintStr = self.encode(hints) {
            FileManager.writeFile(HintService.fileName, contents: hintStr)
        }
    }
    
    static func getHints() -> [Hint]? {
        if let data: Data = FileManager.readFile(HintService.fileName) {
            return self.decode(data)
        }
        return nil
    }
    
    static func getHints(for screen: String) -> [Hint]? {
        if let data: Data = FileManager.readFile(HintService.fileName),
            let hints = self.decode(data) {
            return hints.filter({ (hint) -> Bool in
                return hint.screen == screen && hint.seen == false
            })
        }
        return nil
    }
    
    static func getHint(with message: String) -> Hint? {
        if let data: Data = FileManager.readFile(HintService.fileName),
            let hints = self.decode(data) {
            return hints.filter({ (hint) -> Bool in
                return hint.message == message
            }).first
        }
        return nil
    }
    
    static func setHintSeen(with message: String) {
        if let data: Data = FileManager.readFile(HintService.fileName),
            var hints = self.decode(data) {
            if let index  = hints.lastIndex(where: { (hint) -> Bool in
                return hint.message == message
            }) {
                hints[index].seen = true
                HintService.storeHints(hints)
            }
        }
    }
}
