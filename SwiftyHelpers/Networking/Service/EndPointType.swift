//
//  EndPointType.swift
//  Fishing Nets
//
//  Created by Timothy Barnard on 03/08/2018.
//  Copyright © 2018 Timothy Barnard. All rights reserved.
//

import Foundation

public protocol EndPointType {
    var baseURL: URL { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var task: HTTPTask { get }
    var headers: HTTPHeaders? { get }
}
